<?php
 session_start();
 error_reporting(0); # ปิดแจ้งเตื่อน erro ทุกอย่าง
 if($_SESSION["emp_id"] == "")
 {
  	echo "<script>";
  	echo "alert(\"กรุณาล็อกอิน\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 elseif($_SESSION['status']!="Manager")
 {
  	echo "<script>";
  	echo "alert(\"page นี้สำหรับ Manager เท่านั้น!\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 include_once("../../db.inc.php");

 $sql = "SELECT * FROM employee Where emp_id = ' ".$_SESSION['emp_id']." '" ;
 $query = mysqli_query($connect,$sql);
 $result = mysqli_fetch_array($query);

 $id = $_GET['cus_id'];
 $sql4 = "SELECT*FROM customer  Where cus_id = '$id'";
 $query4 = mysqli_query($connect,$sql4);
 $show  = mysqli_fetch_array($query4);
 //$id = $_GET['repair_id'];
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>IT - Support</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" href="../../css/styless.css">
<!-- google font -->
<link href="https://fonts.googleapis.com/css?family=Aclonica" rel="stylesheet">
<!-- script bootstrap-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script>
      $(document).ready(function() {
        $('#datatable').DataTable( {
             "lengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]], //แสดงข้อมูลแถวที่ต้องการ เช่น 5 รายการ 10 25 50 100 และแสดงทั้และแสดงทั้งหมด
              //ปรับภาษา
             "oLanguage": {
             "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
             "sZeroRecords": "ไม่มีข้อมูลที่ค้นหา",
             "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
             "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
             "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
             "sSearch": "ค้นหา :",
            }
            } );
        } );
</script>
</head>
<body>

 <?php include("header.php");?>
 <div class="container-fluid" >
 	<div class="row">
 	<?php include("menu.php");?>
 	<div class="mt-2 col-xs-8 col-sm-8 col-md-8 col-lg-8 border " style="background: #FFFFFF;">
 	<div class="py-1"></div>
 	  <div class="font-weight-bold">
      <h5><u> แบบฟอร์มแจ้งซ่อมคอมพิวเตอร์</u>
     </h5>
 		</div>
 		<hr class="mt-1" style="background: goldenrod;">
 		<div>
 		<!-- ส่วนเนื้อหา ไม่มีไรให้ใส่-->

    <div class="py-1"></div>
      <div class="mx-auto">
      <form  method ="post" class="mr-auto ml-auto ">
      <div class="form-row">
        <div class="mt-1 form-grop col-xs-12 col-sm-12 col-md-12"></div>
             <div class=" form-grop col-xs-2 col-sm-2 col-md-2">
               <label>รหัสลูกค้า:</label>
               <input type="text" class="form-control" name="id"   value="<?php echo $show['cus_id'];?>"disabled>
             </div>

             <div class="mt-1 form-grop col-xs-10 col-sm-10 col-md-10">

             </div>

             <hr class="mt-5" style="border-style: dotted; background:#9C0E10; ">

             <div class="mt-2 form-group  col-xs-3 col-sm-3 col-md-3">
                  <label>ประเภทสินค้า:</label>
                  <input type="text" class="form-control" name="cat"   placeholder="ประเภทสินค้า" Required>
             </div>

             <div class="mt-2  form-group col-xs-3 col-sm-3 col-md-3">
             	  <label>ยี่ห้อสินค้า:</label>
             	  <input type="text" class="form-control" name="brand" placeholder="ยี่ห้อสินค้า" Required>
             </div>

             <div class="mt-2  form-group col-xs-3 col-sm-3 col-md-3">
             	  <label>รุ่น:</label>
             	  <input type="text" class="form-control" name="version" placeholder="ยี่ห้อสินค้า" Required>
             </div>

              <div class="mt-2 form-group col-xs-3 col-sm-3 col-md-3">
    			  <label>จำนวนสินค้า <span style="color: red;">(*เครื่อง) </span> :</label>
             	  <input type="number" class="form-control" name="amount" placeholder="จำนวนสินค้า" Required>
             </div>

             <div class="form-group col-xs-6 col-sm-6 col-md-6">
             	  <label>วันที่แจ้งซ่อม :</label>
             	  <input type="date" class="form-control" name="date" placeholder="จำนวนสินค้า" Required>
             </div>

             <div class="form-group  col-xs-12 col-sm-12 col-md-12">
                  <label>ปัญหา:</label>
                  <textarea class="form-control" name="prd" id="exampleFormControlTextarea1" rows="3" placeholder="ทีอยู่"></textarea>
             </div>



             <div class="form-group col-xs-9 col-sm-9 col-md-9">

             </div>

             <div class="py-3 mr-auto col-xs-8 col-sm-8 col-md-8">
                 <div class=" mr-auto">
                     <button type="submit" name="submit" class="btn btn-success">Save</button>
                     <button type="reset" name="reset" class="btn btn-danger">Reset</button>
                 </div>
             </div>
      </form>
    </div>

		</div>
	</div>
 	</div>
 </div>


 <?php
  // insertข้อมูล
  if(isset($_POST['submit']))
  {

    $id = $_GET['cus_id'];
    #ตาราง product
    $category = mysqli_real_escape_string($connect,$_POST['cat']);		# ประเภทสินค้า
    $brand = mysqli_real_escape_string($connect,$_POST['brand']);			# ยี่ห้อสินค้า
    $version = mysqli_real_escape_string($connect,$_POST['version']);		# รุ่น
    $amount = mysqli_real_escape_string($connect,$_POST['amount']);		# จำนวนสินค้า

    #ตาราง repair
    $rand = sprintf("%04d", rand(0,9999));
    $order_no = $rand;
    $problem = mysqli_real_escape_string($connect,$_POST['prd']);
    $employee = mysqli_real_escape_string($connect,$_SESSION["emp_id"]); #รหัสพนักงาน
    $strdate = mysqli_real_escape_string($connect,$_POST['date']);


    $sqls  = "INSERT INTO product";
    $sqls .= "(category, brand, version, amount)";
    $sqls .= "VALUES('$category', '$brand', '$version', '$amount')";
    $querys = mysqli_multi_query($connect,$sqls) or die(mysqli_error($connect));
    $product_id = mysqli_insert_id($connect);

    $customer = $id;
    $product = $product_id;
    $sqls   = "INSERT INTO repair";
    $sqls  .= "(order_no, customer, product, employee, problem, strdate)";
    $sqls  .= "VALUES('$order_no','$customer','$product', '$employee','$problem','$strdate')";
    $querys = mysqli_multi_query($connect,$sqls) or die (mysqli_error($connect));

    if($querys)
    {
      echo "<script type='text/javascript'>";
      echo "alert('Insert Succesfuly');";
      echo "window.location = 'Waiting_from.php?signup=succes'; ";
      echo "</script>";
    }
   else
   {
     echo "<script type='text/javascript'>";
     echo "alert('Error back to Update again');";
     echo "window.history.back()";
     echo "</script>";
   }

   mysqli_close($connect);
  }

 ?>

 <div class="mt-5"></div>
</body>
</html>
