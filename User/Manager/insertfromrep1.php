<?php
 session_start();
 error_reporting(0);
 require("../../db.inc.php");

 #ตาราง Customer
 $fname = mysqli_real_escape_string($connect,$_POST['name']); 			# ชื่อ
 $lname = mysqli_real_escape_string($connect,$_POST['lname']); 		# นามสกุล
 $tell  = mysqli_real_escape_string($connect,$_POST['tell']);  		# เบอร์โทร
 $address = mysqli_real_escape_string($connect,$_POST['adr']);			# ที่อยู่
 $subdistrict = mysqli_real_escape_string($connect,$_POST['sub']);		# ตำบล
 $district = mysqli_real_escape_string($connect,$_POST['district']);	# อำเภอ
 $province = mysqli_real_escape_string($connect,$_POST['prc']);		# จังหวัด
 $zipcode = mysqli_real_escape_string($connect,$_POST['_code']); 		# รหัสไปรษณีย์

 #ตาราง product
 $category = mysqli_real_escape_string($connect,$_POST['cat']);		# ประเภทสินค้า
 $brand = mysqli_real_escape_string($connect,$_POST['brand']);			# ยี่ห้อสินค้า
 $version = mysqli_real_escape_string($connect,$_POST['version']);		# รุ่น
 $amount = mysqli_real_escape_string($connect,$_POST['amount']);		# จำนวนสินค้า


 #ตาราง repair
 $rand = sprintf("%04d", rand(0,9999));
 $order_no = $rand;
 $problem = mysqli_real_escape_string($connect,$_POST['prd']);
 $employee = mysqli_real_escape_string($connect,$_SESSION["emp_id"]); #รหัสพนักงาน
 $strdate = mysqli_real_escape_string($connect,$_POST['date']);





 $sql  = "INSERT INTO customer";
 $sql .= "(fname,lname,tell,address,subdistrict,district,province,zipcode)";
 $sql .= "VALUES('$fname', '$lname','$tell','$address','$subdistrict','$district','$province','$zipcode')";
 $query = mysqli_multi_query($connect,$sql);
 $cus_id = mysqli_insert_id($connect);
 $sql  = "INSERT INTO product";
 $sql .= "(category, brand, version, amount)";
 $sql .= "VALUES('$category', '$brand', '$version', '$amount')";
 $query = mysqli_multi_query($connect,$sql) or die(mysqli_error($connect));
 $product_id = mysqli_insert_id($connect);

 $customer = $cus_id;
 $product = $product_id;
 $sql   = "INSERT INTO repair";
 $sql  .= "(order_no, customer, product, employee, problem, strdate)";
 $sql  .= "VALUES('$order_no','$customer','$product', '$employee','$problem','$strdate')";
 $query = mysqli_multi_query($connect,$sql) or die (mysqli_error($connect));

 if($query)
 {
   echo "<script type='text/javascript'>";
   echo "alert('Insert Succesfuly');";
   echo "window.location = 'Waiting_from.php?signup=succes'; ";
   echo "</script>";
 }
else
{
  echo "<script type='text/javascript'>";
  echo "alert('Error back to Update again');";
  echo "window.history.back()";
  echo "</script>";
}

mysqli_close($connect);
?>
