<!doctype html>
<div class="mt-2 ml-5 col-xs-3 col-sm-3 col-md-3 col-lg-3 ">
    <ul class="list-group">

      <li class="list-group-item d-flex justify-content-between align-items-center">
        <a href="insert_Employee.php" style="color:goldenrod;"> <span>
       <span>เพิ่มข้อมูลพนักงาน</span>
       </a>
      </li>


      <li class="list-group-item d-flex justify-content-between align-items-center">
       <a href="insert_repair.php" style="color:goldenrod;"> <span>
       <span>แจ้งซ่อมคอมพิวเตอร์</span>
       </a>
      </li>


    <div class="panel-group">
        <div class="panel panel-default">
            <li class="list-group-item d-flex justify-content-between align-items-center active">
                <a data-toggle="collapse" href="#collapse1">
                 <span style="color:aliceblue;">
                  จัดการข้อมูล
				 </span>
                </a>
            </li>
        <div id="collapse1" class="panel-collapse collapse">
            <ul class="list-group">

                <li class="list-group-item">
                <a href="view_employee.php" style="color:goldenrod;">
                Employee
                </a>
                </li>
                <li class="list-group-item">
                <a href="view_technician.php"style="color:goldenrod;">
                Technician
                </a>
                </li>
                <li class="list-group-item">
                <a href="view_customer.php"style="color:goldenrod;">
                Customer
                </a>
                </li>

            </ul>
        </div>
        </div>

        <div class="panel-group">
            <div class="panel panel-default">
                <li class="list-group-item d-flex justify-content-between align-items-center active" style="background:#3f4750;border-color:#c7c7c7;">
                    <a data-toggle="collapse" href="#collapse2">
                     <span style="color:aliceblue;">
                      รายงานการซ่อม
             </span>
                    </a>
                </li>
            <div id="collapse2" class="panel-collapse collapse">
                <ul class="list-group">

                  <li class="list-group-item d-flex justify-content-between align-items-center">
                  <a href="Waiting_from.php" style="color:goldenrod;">
                  ข้อมูลคอมพิวเตอร์ที่รอซ่อม
                  </a>

                  <li class="list-group-item d-flex justify-content-between align-items-center">
                  <a href="repaired_from.php" style="color:goldenrod;">
                  ข้อมูลคอมพิวเตอร์ที่กำลังซ่อม
                  </a>
                </li>

                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                <a href="completed_from.php" style="color:goldenrod;">
                ข้อมูลคอมพิวเตอร์ที่ซ่อมเสร็จแล้ว
                </a>
              </li>

                </ul>
            </div>
            </div>
        </div>
    </div>

    </ul>
       <br>
        <p class="ml-2" style="color: #C0BDBD;">
    © Copyright 2018 IT - Support</p>
</div>
