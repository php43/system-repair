﻿<?php
 session_start();
 error_reporting(0);
 if($_SESSION["emp_id"] == "")
 {
  	echo "<script>";
  	echo "alert(\"กรุณาล็อกอิน\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 elseif($_SESSION['status']!="Manager")
 {
  	echo "<script>";
  	echo "alert(\"page นี้สำหรับ Manager เท่านั้น!\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 include_once("../../db.inc.php");
 $sql = "SELECT * FROM employee Where emp_id = ' ".$_SESSION['emp_id']." '" ;
 $query = mysqli_query($connect,$sql);
 $result = mysqli_fetch_array($query);

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" href="../../css/styless.css">
<!-- google font -->
<link href="https://fonts.googleapis.com/css?family=Aclonica" rel="stylesheet">
<!-- script bootstrap-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<title>IT - Support</title>
</head>

<body>
 <?php include "header.php";?>
 <div class="container-fluid" >
 	<div class="row">
 	<?php include("menu.php");?>
 	<div class="mt-2 col-xs-8 col-sm-8 col-md-8 col-lg-8 border " style="background: #FFFFFF;">
 	<div class="py-1"></div>
 	  <div class="font-weight-bold">
      <h4>เพิ่มข้อมูลพนักงาน</h4>

 		</div>
 		<hr class="mt-1" style="background: goldenrod;">
 		<div>
 		<?php include("form_insert_emp.php");?>
		</div>
	</div>
 	</div>
 </div>

 <div class="mt-5"></div>
