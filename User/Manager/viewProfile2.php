﻿<?php
 session_start();
 error_reporting(0);
 if($_SESSION['emp_id']=="")
 {
	echo "<script>";
  	echo "alert(\"กรุณาล็อกอิน\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 elseif($_SESSION['status']!="Manager")
 {
	echo "<script>";
  	echo "alert(\"page นี้สำหรับ Manager เท่านั้น!\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 include_once("../../db.inc.php");
 $sql = "SELECT * FROM employee Where emp_id = ' ".$_SESSION['emp_id']." '" ;
 $query = mysqli_query($connect,$sql);
 $result = mysqli_fetch_array($query);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>IT - Suppot</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" href="../../css/styless.css">
<!-- google font -->
<link href="https://fonts.googleapis.com/css?family=Aclonica" rel="stylesheet">
<!-- script bootstrap-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script>
      $(document).ready(function() {
        $('#datatable').DataTable( {
            "searching": false, // ปิดช่องค้นหา
             "lengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]], //แสดงข้อมูลแถวที่ต้องการ เช่น 5 รายการ 10 25 50 100 และแสดงทั้และแสดงทั้งหมด
              //ปรับภาษา
             "oLanguage": {
             "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
             "sZeroRecords": "ไม่มีข้อมูลที่ค้นหา",
             "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
             "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
             "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
             "sSearch": "ค้นหา :",
            }
            } );
        } );
</script>
</head>
<body>

		<?php include "header.php";?>

		<section class="container">
		<div class="row">
 		  <div class="mt-2 col-xs-12 col-sm-12 col-md-12 col-lg-12 border " style="background: #FFFFFF;">
 		   	<div class="py-1"></div>
 		   	<div class="font-weight-bold ">
 		   	<h5 class="">
           <p class="mt-1">
            <img src="../../imgaes/tools.png" width="30" height="30" class="d-inline-block align-top " alt="">
            รายงานการแจ้งซ่อม
               IT - Support
             </p>
        </h5>
			  <hr class="mt-1" style="">
         <!-- ส่วนเนื่อหา-->
         <?php
          $cus_id = $_GET['cus_id'];

          $ql  = "SELECT customer.*,product.*, repair.*\n";
          $ql .= "FROM customer,product,repair\n";
          $ql .= "where product.product_id = repair.product and customer.cus_id = repair.customer and customer.cus_id ='$cus_id'\n";
          //$ql .= "order by repair.strdate DESC LIMIT 0,3";
          /*
          $ql  = "SELECT c.*,r.customer,r.product,r.strdate,r.status,r.problem,r.solution,r.order_no\n";
          $ql .= "FROM customer c, repair r\n";
          $ql .= "WHERE c.cus_id ='$cus_id' and c.cus_id = r.customer  order by r.strdate DESC LIMIT 0,1";
          */
          $query1 = mysqli_query($connect,$ql) or die ("sql error".mysqli_error($connect));
          $query2 = mysqli_query($connect,$ql) or die ("sql error".mysqli_error($connect));
          $num  = mysqli_num_rows($query1);
          $row = mysqli_fetch_array($query1);

         ?>
         <?php if($num <=0){?>
           <div class="mt-5 form-group  col-xs-12 col-sm-12 col-md-12">
                <label><h4 style="background:red;color:white;">ไม่มีข้อมูลการซ่อม</h4></label>
           </div>
       <?php }?>
     <?php if($num >0){?>
       <input type="button" class="btn btn-outline-success" name="Button" value="Print" onclick="javascript:this.style.display='none';window.print();">
       <div class="mt-5 form-group  col-xs-12 col-sm-12 col-md-12">
            <label>ชื่อ - นามสกุล: <?php echo $row['fname'].'  '.$row['lname'];?> </label>
       </div>
       <div class="form-group  col-xs-12 col-sm-12 col-md-12">
            <label>เบอร์โทรศัพท์ <?php echo $row['tell'];?> </label>
       </div>
       <div class="form-group  col-xs-12 col-sm-12 col-md-12">
            <label>ที่อยู่
               <?php echo $row['address'].' ตำบล '.$row['subdistrict'].' อำเภอ '.$row['district']
               .' จังหวัด '.$row['province'].' รหัสไปรษณีย์ '.$row['zipcode'];?>
            </label>
       </div>



          <table  border="0" align="center" cellspacing="1" class="table table-bordered table-hover " id="datatable">
                  <!--ส่วนหัว-->
                  <thead>
                    <tr align="center">

                      <th align="center">วันที่แจ้งซ่อม</th>
                      <th align="center">รหัสสินค้า</th>

                      <th align="center">ปัญหา</th>
                      <th align="center">สถานะ</th>
                      <th align="center">แก้ปัญหา</th>
                      <th align="center">รายละเอียด</th>


                   </tr>
                  </thead>
                  <?php while ($row1 = mysqli_fetch_array($query2)){?>
                    <tr align="center">
                       <td><?php echo date("d/m/Y",strtotime($row1 ['strdate']));?></td>
                       <td><?php echo $row1['product'];?></td>


                       <td><?php echo $row1['problem'];?></td>
                       <td>
                         <?php
                         if($row1 ['status']=='รอซ่อม')
                         {
                           echo "<div class='btn-sm' style='background:#b39911;color:white;'>รอซ่อม</div>";

                         }elseif($row1 ['status']=='กำลังซ่อม'){
                            echo "<div class='btn-sm' style='background:#1176b3; color:white;'>กำลังซ่อม</div>";
                         }else{
                            echo "<div class='btn-sm' style='background:Green;color:white;'>ซ่อมเสร็จแล้ว</div>";
                         }
                         ?>
                       </td>
                       <td><?php
                                  if($row1['solution']=="")
                                  {
                                     echo "-";
                                  }
                                  else{
                                     echo $row1['solution'] ;
                                  }
                                  ;?>
                      </td>
                      <td>
                          <a class="btn-sm btn-outline-info"href="viewProfile3.php?product=<?php echo $row1['product'];?>">View</a>

                      </td>
                      </tr>
                  <?php } ?>
                </table>
  <div>
  </div><br>
       <div class="py-5">
          <h4 class="text-right">
                   ลงชื่อ..........................................................
          <br><br><br>(......................................................................)
          <br><br>เจ้าหน้าที่รับผิดชอบออกใบเสร็จ
        </h4>
       </div>

     <?php }?>
    </div>
  </div>
</div>
		</section>
<div class="mt-5"></div>
  </body>
</html>
