<?php
 session_start();
 error_reporting(0);
 if($_SESSION["emp_id"] == "")
 {
  	echo "<script>";
  	echo "alert(\"กรุณาล็อกอิน\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 elseif($_SESSION['status']!="Employee")
 {
  	echo "<script>";
  	echo "alert(\"page นี้สำหรับ Manager เท่านั้น!\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 include_once("../../db.inc.php");
 $sql = "SELECT * FROM employee Where emp_id = ' ".$_SESSION['emp_id']." '" ;
 $query = mysqli_query($connect,$sql);
 $result = mysqli_fetch_array($query);

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<title>IT - Support</title>
</head>

<body>
 		  <?php
      $empid = $_SESSION['emp_id'];
      $sql2  = "SELECT customer.*,product.*, repair.*\n";
      $sql2 .= "FROM customer,product,repair\n";
      $sql2 .= "where product.product_id = repair.product and customer.cus_id = repair.customer and repair.status ='ซ่อมเสร็จแล้ว'  \n ";
      $sql2 .= "and repair.employee ='$empid'\n";
      $results = mysqli_query($connect,$sql2) or die (mysqli_error($connect));

       ?>
 		<table  border="0" align="center" cellspacing="1" class=" table table-bordered table-hover " id="datatable">
            <!--ส่วนหัว-->
      <thead>
        <tr align="center">
          <th align="center" width="100px">วันที่แจ้งซ่อม</th>
          <th align="center" width="100px">รหัสลูกค้า</th>
          <th align="center" width="100px">ชื่อ - นามสกุล</th>
          <th align="center" width="100px">รหัสสินค้า</th>
          <th align="center" width="200px">รหัสฟอร์มแจ้งซ่อม</th>
          <th align="center" width="10px">View</th>

       </tr>
    </thead>
    <?php while($row = mysqli_fetch_array($results))  {?>
       <tr align="center">
           <td><?php  echo date("d/m/Y",strtotime($row ['strdate']));?></td>
           <td><a href="viewcustomer1.php?cus_id=<?php echo $row['cus_id'];?>" target="_blank"><?php echo $row['cus_id'];?></a></td>
           <td><?php echo $row['fname']." ".$row['lname'];?></td>
           <td><a href="viewproduct.php?product_id=<?php echo $row['product_id'];?>"  target="_blank"><?php echo $row['product'];?></a></td>
           <td><a href="viewrepair.php?repair_id=<?php echo $row['repair_id'];?>"  target="_blank"><?php echo $row['repair_id'];?></a></td>

           <td>
            <a class="btn-sm btn-outline-info"href="viewProfile1.php?repair_id=<?php echo $row['repair_id'];?>"onclick="return confirm('ต้องการดูข้อมูลประวัติการซ่อม')">View</a>
           </td>
        </tr>
            <?php }?>
          </table>
	</body>
</html>
