<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>IT - Suppot</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" href="../../css/styless.css">
<!-- google font -->
<link href="https://fonts.googleapis.com/css?family=Aclonica" rel="stylesheet">
<!-- script bootstrap-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script>
      $(document).ready(function() {
        $('#datatable').DataTable( {
             "lengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]], //แสดงข้อมูลแถวที่ต้องการ เช่น 5 รายการ 10 25 50 100 และแสดงทั้และแสดงทั้งหมด
              //ปรับภาษา
             "oLanguage": {
             "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
             "sZeroRecords": "ไม่มีข้อมูลที่ค้นหา",
             "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
             "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
             "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
             "sSearch": "ค้นหา :",
            }
            } );
        } );
</script>
 <body>
   <?php
   include_once("../../db.inc.php");

   $ql1  = "SELECT customer.*,product.*, repair.*\n";
   $ql1 .= "FROM customer,product,repair\n";
   $ql1 .= "ORDER BY  repair.strdate DESC\n";
   $ql1 .= "where product.product_id = repair.product and customer.cus_id = repair.customer  \n";

   $query2 = mysqli_query($connect,$ql1) or die (mysqli_error($connect));
   ?>


      <table  border="0" align="center" cellspacing="1" class="table table-bordered table-hover " id="datatable">
              <!--ส่วนหัว-->
              <thead>
                <tr align="center">

                  <th align="center">วันที่แจ้งซ่อม</th>
                  <th align="center">รหัสสินค้า</th>
                  <th align="center">ประเภทสินค้า</th>
                  <th align="center">ยี่ห้อสินค้า</th>
                  <th align="center">จำนวนสินค้า</th>
                  <th align="center">ปัญหา</th>
                  <th align="center">สถานะ</th>
                  <th align="center">แก้ปัญหา</th>


               </tr>
              </thead>
              <?php while ($row1 = mysqli_fetch_array($query2)){?>
                <tr align="center">
                   <td><?php echo date("d-m-Y",strtotime($row1 ['strdate']));?></td>
                   <td><?php echo $row1['product_id'];?></td>
                   <td><?php echo $row1['category'];?></td>
                   <td><?php echo $row1['brand'].' รุ่น '.$row1['version'];?></td>
                   <td><?php echo $row1['amount'];?></td>
                   <td><?php echo $row1['problem'];?></td>
                   <td>
                     <?php
                     if($row1 ['status']=='รอซ่อม')
                     {
                       echo "<div class='btn-sm' style='background:#b39911;color:white;'>รอซ่อม</div>";

                     }elseif($row1 ['status']=='กำลังซ่อม'){
                        echo "<div class='btn-info' style='background:#1176b3;'>กำลังซ่อม</div>";
                     }else{
                        echo "<div class='btn' style='background:Green;color:white;'>ซ่อมเสร็จแล้ว</div>";
                     }
                     ?>
                   </td>
                   <td><?php echo $row1['solution'];?></td>
                </tr>
              <?php }?>
            </table>


  </body>
</html>
