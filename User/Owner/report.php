<?php
 session_start();
 error_reporting(0);
 if($_SESSION["emp_id"] == "")
 {
  	echo "<script>";
  	echo "alert(\"กรุณาล็อกอิน\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 elseif($_SESSION['status']!="Owner")
 {
  	echo "<script>";
  	echo "alert(\"page นี้สำหรับ Owner เท่านั้น!\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 include_once("../../db.inc.php");
 $sql = "SELECT * FROM employee Where emp_id = ' ".$_SESSION['emp_id']." '" ;
 $query = mysqli_query($connect,$sql);
 $result = mysqli_fetch_array($query);

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" href="../../css/styless.css">
<!-- google font -->
<link href="https://fonts.googleapis.com/css?family=Aclonica" rel="stylesheet">
<!-- script bootstrap-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script>
      $(document).ready(function() {
        $('#datatable').DataTable( {
             "lengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]], //แสดงข้อมูลแถวที่ต้องการ เช่น 5 รายการ 10 25 50 100 และแสดงทั้และแสดงทั้งหมด
              //ปรับภาษา
             "oLanguage": {
             "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
             "sZeroRecords": "ไม่มีข้อมูลที่ค้นหา",
             "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
             "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
             "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
             "sSearch": "ค้นหา :",
            }
            } );
        } );
</script>
<title>IT - Support</title>
</head>

<body>
 <?php include "header.php";?>
 <div class="container-fluid" >
 	<div class="row">
 	<?php include("menu.php");?>
 	<div class="mt-2 col-xs-8 col-sm-8 col-md-8 col-lg-8 border " style="background: #FFFFFF;">
 	<div class="py-1"></div>
 	  <div class="font-weight-bold">
 		<h4>รายงานการแจ้งซ่อมคอมพิวเตอร์</h4>
 	  </div>
 		<hr class="mt-1" style="background: goldenrod;">
 		<div class="mt-2">
 		  <?php
            //   $result1 = mysqli_query($connect,"SELECT*FROM employee  where status ='Technician' ");
            //   $empid = $_SESSION['emp_id'];
              $sql2  = "SELECT customer.*,product.*, repair.*\n";
              $sql2 .= "FROM customer,product,repair\n";
              $sql2 .= "where product.product_id = repair.product and customer.cus_id = repair.customer\n ";
              //$sql2 .= "and repair.technician ='$empid'\n";
              //$sql2 .= "order by repair.repair_id DESC ";
              $results1 = mysqli_query($connect,$sql2) or die (mysqli_error($connect));
           ?>
 		<table  border="0" align="center" cellspacing="1" class="table table-bordered table-hover " id="datatable">
            <!--ส่วนหัว-->
            <thead>
              <tr align="center">

                <th align="center"width="15px">วันที่แจ้งซ่อม</th>
                <th align="center">รหัสลูกค้า</th>
                <th align="center"width="25px">ชื่อ-นามสกุล</th>
                <th align="center" width="100px">รหัสสินค้า</th>
                <th align="center">รหัสฟอร์มแจ้งซ่อม</th>

             </tr>
            </thead>
              <?php while($row = mysqli_fetch_array($results1))  {?>
              <tr align="center">
                 <td><?php  echo date("d/m/Y",strtotime($row ['strdate']));?></td>
                 <td><a href="viewcustomer1.php?cus_id=<?php echo $row['cus_id'];?>" target="_blank"><?php echo $row['cus_id'];?></td>
                 <td><?php echo $row['fname'].' '.$row['lname'];?></td>
                 <td><a href="viewproduct.php?product_id=<?php echo $row['product_id'];?>"  target="_blank"><?php echo $row['product'];?></a></td>
                 <td><a href="viewrepair.php?repair_id=<?php echo $row['repair_id'];?>"  target="_blank"><?php echo $row['repair_id'];?></a></td>
              </tr>
            <?php }?>
          </table>
		</div>
		</div>
 	</div>
 </div>

<div class="mt-5"></div>
