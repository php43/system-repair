<!doctype html>
<div class="mt-2 ml-5 col-xs-3 col-sm-3 col-md-3 col-lg-3 ">
    <ul class="list-group">
        <li class="list-group-item d-flex justify-content-between align-items-center active ">
            <a href="Owner_page_insert_emp.php"> 
            <span style="color:aliceblue;">
            เพิ่มข้อมูลพนักงาน 
			</span>
            </a>
        </li>
        
    <div class="panel-group">
        <div class="panel panel-default">
            <li class="list-group-item d-flex justify-content-between align-items-center">
                <a data-toggle="collapse" href="#collapse1">
                 <span style="color:goldenrod;">
                  จัดการข้อมูล 
				 </span>
                </a>
            </li>
        <div id="collapse1" class="panel-collapse collapse">
            <ul class="list-group">
                <li class="list-group-item">
                <a href="edit_manager.php" style="color:goldenrod;">
                Manager
                </a>
                </li>
                
                <li class="list-group-item">
                <a href="edit_emp.php" style="color:goldenrod;">
                Employee
                </a>
                </li>
                <li class="list-group-item">
                <a href="edit_technician.php"style="color:goldenrod;">
                Technician
                </a>
                </li>
                <li class="list-group-item">
                <a href="view_customer.php"style="color:goldenrod;">
                Customer
                </a>
                </li>
            </ul>
        </div>
        </div>
    </div>

    <div class="panel-group">
            <div class="panel panel-default">
                <li class="list-group-item d-flex justify-content-between align-items-center ">
                    <a data-toggle="collapse" href="#collapse2">
                     <span style="color:goldenrod;">
                     จัดการข้อมูลการซ่อมคอมพิวเตอร์ 

             </span>
                    </a>
                </li>
            <div id="collapse2" class="panel-collapse collapse">
                <ul class="list-group">

                  <li class="list-group-item d-flex justify-content-between align-items-center">
                  <a href="Waiting_from.php" style="color:goldenrod;">
                  ข้อมูลคอมพิวเตอร์ที่รอซ่อม
                  </a>

                  <li class="list-group-item d-flex justify-content-between align-items-center">
                  <a href="repaired_from.php" style="color:goldenrod;">
                  ข้อมูลคอมพิวเตอร์ที่กำลังซ่อม
                  </a>
                </li>

                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                <a href="completed_from.php" style="color:goldenrod;">
                ข้อมูลคอมพิวเตอร์ที่ซ่อมเสร็จแล้ว
                </a>
              </li>

                </ul>
            </div>
            </div>
        </div>


        <li class="list-group-item d-flex justify-content-between align-items-center">
            <a href="report.php"  style="color:goldenrod;">รายงานการแจ้งซ่อมคอมพิวเตอร์ 
            </a> 
        </li>
    </ul>
    <br>
    <p class="ml-2" style="color: #C0BDBD;">
    © Copyright 2018 IT - Support</p>
</div>


