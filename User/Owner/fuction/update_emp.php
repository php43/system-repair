<?php

    if(isset($_POST["submit"]))
    {
		$username = mysqli_real_escape_string($connect,$_POST['us']);
		$password = mysqli_real_escape_string($connect,$_POST['pwd']);
		$fname = mysqli_real_escape_string($connect,$_POST['name']);
		$lname = mysqli_real_escape_string($connect,$_POST['lname']);
		$sex    = mysqli_real_escape_string($connect,$_POST['_Sex']);
		$tell  = mysqli_real_escape_string($connect,$_POST['tell']); # เบอร์โทรศัพท์
		$email = mysqli_real_escape_string($connect,$_POST['email']); # อีเมล์
		$address = mysqli_real_escape_string($connect,$_POST['adr']); # ที่อยู่
		$subdistrict = mysqli_real_escape_string($connect,$_POST['sub']); # ตำบล
		$district = mysqli_real_escape_string($connect,$_POST['district']); # อำเภอ
		$province = mysqli_real_escape_string($connect,$_POST['prc']); #จังหวัด
		$zipcode = mysqli_real_escape_string($connect,$_POST['_code']); # รหัสไปรษณีย์
 		$status  = mysqli_real_escape_string($connect,$_POST['status']);

		$sql ="UPDATE employee SET
                        username = ?,
                        password = ?,
                        fname =?,
                        lname =?,
						sex =?,
                        tell  =?,
                        email =?,
                        address =?,
                        subdistrict =?,
                        district=?,
                        province=?,
                        zipcode=?,
                        status =?
                        where emp_id =?";

                    if($stmt = mysqli_prepare($connect,"$sql")){
                      mysqli_stmt_bind_param($stmt ,"ssssssssssssss"
                      ,$username ,$password, $fname ,$lname, $sex,
					 $tell,$email ,$address ,$subdistrict ,$district,$province,$zipcode,$status,$emp_id);
                      mysqli_stmt_execute($stmt);
                      if($stmt){
                      echo "<script type='text/javascript'>";
                      echo "alert('Update Succesfuly');";
											echo "window.history.back()";
                      //echo "window.location = '../Owner_page.php?signup=succes'; ";
                      echo "</script>";
                    }else{
                      echo "<script type='text/javascript'>";
                      echo "alert('Error back to Update again');";
                      //echo "window.location = 'Owner_page.php?signup=Error'; ";
                      echo "</script>";
                    }
                    }
                  }
                  mysqli_close($connect);
            ?>
