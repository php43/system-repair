<?php
 session_start();
 error_reporting(0);
 if($_SESSION['emp_id']=="")
 {
	echo "<script>";
  	echo "alert(\"กรุณาล็อกอิน\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 elseif($_SESSION['status']!="Owner")
 {
	echo "<script>";
  	echo "alert(\"page นี้สำหรับ Owner เท่านั้น!\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 include_once("../../db.inc.php");
 $sql = "SELECT * FROM employee Where emp_id = ' ".$_SESSION['emp_id']." '" ;
 $query = mysqli_query($connect,$sql);
 $result = mysqli_fetch_array($query);

 $cus_id  = $_GET["cus_id"];
 $query2  = mysqli_query($connect,"SELECT * FROM customer where cus_id = '$cus_id' ");
 $result2 = mysqli_fetch_array($query2);
?>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<link rel="stylesheet" href="../../css/styless.css">
		<link href="https://fonts.googleapis.com/css?family=Aclonica" rel="stylesheet">
		<title>IT - Support</title>
	</head>
	<body>

    <?php include("header.php");?>
    <div class="container-fluid" >
    	<div class="row">
    	<?php include("menu.php");?>
    	<div class="mt-2 col-xs-8 col-sm-8 col-md-8 col-lg-8 border " style="background: #FFFFFF;">
    	<div class="py-1"></div>
    		<div>
    		<!-- ส่วนเนื้อหา ไม่มีไรให้ใส่-->
        <div class="py-1"></div>
          <div class="mx-auto">
          <form action="" method ="post" class="mr-auto ml-auto ">
          <div class="form-row">
            <div class="mt-1 form-grop col-xs-12 col-sm-12 col-md-12"></div>

                 <div class=" form-grop col-xs-12 col-sm-12 col-md-12">
                   <h5><u> แบบฟอร์มแก้ไขข้อมูลลูกค้า</u>
                  </h5>
                    		<hr class="mt-1" style="background: goldenrod;">
                 </div>



                 <div class="mt-3 form-group  col-xs-4 col-sm-4 col-md-4">
                      <label>ชื่อ:</label>
                      <input type="text" class="form-control" name="name" placeholder="ชื่อ" value="<?php echo $result2['fname'];?>">
                 </div>

                 <div class="mt-3 form-group  col-xs-4 col-sm-4 col-md-4">
                      <label>นามสกุล:</label>
                      <input type="text" class="form-control" name="lname" placeholder="นามสกุล" value="<?php echo $result2['lname'];?>">
                 </div>



                 <div class="mt-3 form-group  col-xs-4 col-sm-4 col-md-4">
                      <label> เบอร์โทรศัพท์:</label>
                      <input type="tell" class="form-control" name="tell"  maxlength="10" placeholder="เบอร์โทรศัพท์" value="<?php echo $result2['tell'];?>">
                 </div>

                 <div class="form-group  col-xs-12 col-sm-12 col-md-12">
                      <label>ที่อยู่:</label>
                      <textarea class="form-control" name="adr" id="exampleFormControlTextarea1" rows="3" placeholder="ทีอยู่"><?php echo $result2['address'];?></textarea>
                 </div>

                 <div class="form-group  col-xs-3 col-sm-3 col-md-3">
                      <label>ตำบล:</label>
                      <input type="text" class="form-control" name="sub"   placeholder="ชื่อตำบล" value="<?php echo $result2['subdistrict'];?>">
                 </div>

                 <div class="form-group  col-xs-3 col-sm-3 col-md-3">
                      <label>อำเภอ:</label>
                      <input type="text" class="form-control" name="district"   placeholder="ชื่ออำเภอ" value="<?php echo $result2['district'];?>">
                 </div>

                 <div class="form-group  col-xs-3 col-sm-3 col-md-">
                      <label>จังหวัด:</label>
                      <input type="text" class="form-control" name="prc"   placeholder="ชื่อจังหวัด" value="<?php echo $result2['province'];?>">
                 </div>

                 <div class="form-group  col-xs-3 col-sm-3 col-md-3">
                      <label>รหัสไปรษณีย์:</label>
                      <input type="text" class="form-control" name="_code"  maxlength="5" placeholder="รหัสไปรษณีย์" value="<?php echo $result2['zipcode'];?>">

                 </div>
                 <div class="form-group col-xs-9 col-sm-9 col-md-9">

                 </div>
                 <div class="py-3 mr-auto col-xs-8 col-sm-8 col-md-8">
                     <div class=" mr-auto">
                         <button type="submit" name="submit" class="btn btn-success">Save</button>
                         <button type="reset" name="reset" class="btn btn-danger">Reset</button>
                     </div>
                 </div>
          </form>
        </div>
   		</div>
   	</div>
    	</div>
    </div>

    <?php

    if(isset($_POST["submit"]))
    {
      $fname = mysqli_real_escape_string($connect,$_POST['name']); 			# ชื่อ
      $lname = mysqli_real_escape_string($connect,$_POST['lname']); 		# นามสกุล
      $tell  = mysqli_real_escape_string($connect,$_POST['tell']);  		# เบอร์โทร
      $address = mysqli_real_escape_string($connect,$_POST['adr']);			# ที่อยู่
      $subdistrict = mysqli_real_escape_string($connect,$_POST['sub']);		# ตำบล
      $district = mysqli_real_escape_string($connect,$_POST['district']);	# อำเภอ
      $province = mysqli_real_escape_string($connect,$_POST['prc']);		# จังหวัด
      $zipcode = mysqli_real_escape_string($connect,$_POST['_code']); 		# รหัสไปรษณีย์

		$update1 ="UPDATE customer SET
                        fname =?,
                        lname =?,
                        tell  =?,
                        address =?,
                        subdistrict =?,
                        district=?,
                        province=?,
                        zipcode=?
                        where cus_id =?";

                    if($stmt = mysqli_prepare($connect,"$update1")){
                      mysqli_stmt_bind_param($stmt ,"sssssssss"
                      ,$fname ,$lname,$tell,$address ,$subdistrict ,$district,$province,$zipcode,$cus_id);
                      mysqli_stmt_execute($stmt);
                      if($stmt){
                        echo "<script type='text/javascript'>";
                        echo "alert('Update Succesfuly');";
                        echo "window.location = 'view_customer.php?signup=succes'; ";
                        echo "</script>";
                    }else{
                      echo "<script type='text/javascript'>";
                      echo "alert('Error back to Update again');";
                      echo "window.history.back()";
                      echo "</script>";
                    }
                    }
                  }
                  mysqli_close($connect);
            ?>



    <div class="mt-5"></div>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</body>
</html>
