﻿<?php
 session_start();
 error_reporting(0);
 if($_SESSION["emp_id"] == "")
 {
  	echo "<script>";
  	echo "alert(\"กรุณาล็อกอิน\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 elseif($_SESSION['status']!="Owner")
 {
  	echo "<script>";
  	echo "alert(\"page นี้สำหรับ Owner เท่านั้น!\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 include_once("../../db.inc.php");
 $sql = "SELECT * FROM employee Where emp_id = ' ".$_SESSION['emp_id']." '" ;
 $query = mysqli_query($connect,$sql);
 $result = mysqli_fetch_array($query);

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>IT - Supprot</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <link rel="stylesheet" href="../../css/styless.css">
  <link href="https://fonts.googleapis.com/css?family=Aclonica" rel="stylesheet">
  </head>

  <body>
    <?php
    $repair_id  = $_GET["repair_id"];
    $query2  = mysqli_query($connect,"SELECT * FROM repair  where repair_id = '$repair_id' ")or die(mysqli_error($connect));
    ?>
    <table class="mt-5 mr-auto ml-auto table table-bordered table-hover" style="width:1024px;">
      <thead>
      <tr class="text-center">
        <th>ลำดับ</th>
        <th>รหัสสินค้า</th>
        <th>ปัญหา</th>
        <th>วันที่ซ่อม</th>
        <th>สถานะ<span style="color:red;">(*เครื่อง)</span></th>
      <tr>
      </thead>
     <?php while($row = mysqli_fetch_array($query2)){?>
      <tr class="text-center" >

           <td><?php echo $row ['repair_id'];?></td>
           <td><?php echo $row ['order_no'];?></td>
           <td><?php echo $row ['problem'];?></td>
           <td><?php echo date("d-m-Y",strtotime($row ['strdate']));?></td>
           <td>
             <?php
             if($row ['status']=='รอซ่อม')
             {
               echo "<div class='btn-sm' style='background:#b39911;color:white;'>รอซ่อม</div>";

             }elseif($row ['status']=='กำลังซ่อม'){
                echo "<div class='btn-info' style='background:#1176b3;'>กำลังซ่อม</div>";
             }else{
                echo "<div class='btn' style='background:Green;color:white;'>ซ่อมเสร็จแล้ว</div>";
             }
             ?>
           </td>



      </tr>
      <?php }?>
    </table>
  </body>
</html>
