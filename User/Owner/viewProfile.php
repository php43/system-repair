<?php
 session_start();
 error_reporting(0);
 if($_SESSION['emp_id']=="")
 {
	echo "<script>";
  	echo "alert(\"กรุณาล็อกอิน\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 elseif($_SESSION['status']!="Owner")
 {
	echo "<script>";
  	echo "alert(\"page นี้สำหรับ Owner เท่านั้น!\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";	
 }
 include_once("../../db.inc.php");
 $sql = "SELECT * FROM employee Where emp_id = ' ".$_SESSION['emp_id']." '" ;
 $query = mysqli_query($connect,$sql);
 $result = mysqli_fetch_array($query);

 $emp_id  = $_GET["emp_id"];
 $query2  = mysqli_query($connect,"SELECT * FROM employee where emp_id = '$emp_id' ");
 $result2 = mysqli_fetch_array($query2);
?>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

		<link rel="stylesheet" href="../../css/styless.css">
		<link href="https://fonts.googleapis.com/css?family=Aclonica" rel="stylesheet">

		<title>IT - Support</title>
	</head>
	<body>
		<?php include "header.php";?>
		
		<section class="container">
		 <div class="row">
 		  <div class="mt-2 col-xs-12 col-sm-12 col-md-12 col-lg-12 border " style="background: #FFFFFF;">
 		   	<div class="py-1"></div>
 		   	<div class="font-weight-bold"> 
 		   		ข้อมูลส่วนตัวของ <?php echo $result2['fname']." ".$result2['lname'];?> </div>
			<hr class="mt-1" style="background: goldenrod;">
			  <form action="" method ="post" class="mr-auto ml-auto ">
            <div class="form-row">

             <div class=" mt-2 form-grop  col-xs-12 col-sm-12 col-md-12 ">
             <lable  >เพศ : </lable>
     <input type="radio" name="_Sex" value="M" <?php if($result2["sex"]=="M"){echo"checked";}?> disabled> ชาย
	 <input type="radio" name="_Sex" value="F"<?php if($result2["sex"]=="F"){echo"checked";}?> disabled> หญิง
             </div>




              <div class="mt-2 form-group  col-xs-6 col-sm-6 col-md-6">
                <label>ชื่อ:</label>
                <input type="text" class="form-control" name="name" placeholder="ชื่อ" Required value="<?php echo $result2['fname'];?>" readonly>
              </div>

              <div class="mt-2 form-group  col-xs-6 col-sm-6 col-md-6">
                <label>นามสกุล:</label>
                <input type="text" class="form-control" name="lname" placeholder="นามสกุล" Required  value="<?php echo $result2['lname'];?>" readonly>
              </div>

              <div class="form-group  col-xs-6 col-sm-6 col-md-6">
                <label>อีเมล์:</label>
                <input type="email" class="form-control" name="email" placeholder="อีเมล์" Required value="<?php echo $result2['email'];?>" readonly>
              </div>

              <div class="form-group  col-xs-6 col-sm-6 col-md-6">
                <label> เบอร์โทรศัพท์:</label>
                <input type="tell" class="form-control" name="tell"  maxlength="10" placeholder="เบอร์โทรศัพท์" Required value="<?php echo $result2['tell'];?>" readonly>
              </div>

              <div class="form-group  col-xs-12 col-sm-12 col-md-12">
                <label>ที่อยู่:</label>
                <textarea class="form-control" name="adr" id="exampleFormControlTextarea1" rows="3" placeholder="ทีอยู่" readonly> <?php echo $result2['address'];?></textarea>
              </div>
              <div class="form-group  col-xs-6 col-sm-6 col-md-6">
                <label>ตำบล:</label>
                <input type="text" class="form-control" name="sub"   placeholder="ชื่อตำบล" Required value="<?php echo $result2['subdistrict'];?>"readonly>
              </div>

              <div class="form-group  col-xs-6 col-sm-6 col-md-6">
                <label>อำเภอ:</label>
                <input type="text" class="form-control" name="district"   placeholder="ชื่ออำเภอ" Required value="<?php echo $result2['district'];?>" readonly>
              </div>
              <div class="form-group  col-xs-6 col-sm-6 col-md-6">
                <label>จังหวัด:</label>
                <input type="text" class="form-control" name="prc"   placeholder="ชื่อจังหวัด" Required value="<?php echo $result2['province'];?>" readonly>
              </div>
              <div class="form-group  col-xs-6 col-sm-6 col-md-6">
                <label>รหัสไปรษณีย์:</label>
                <input type="text" class="form-control" name="_code"  maxlength="5" placeholder="รหัสไปรษณีย์" Required value="<?php echo $result2['zipcode'];?>" readonly>
              </div>

              <div class=" form-group  col-xs-12 col-sm-12 col-md-12">
                  <label for="sel1">สถานะ:</label>
                      <input type="text" class="form-control" name="Status" placeholder="สถานะ" value="<?php echo $result2['status'];?>" readonly>
                </div>


              <div class="py-3 mr-auto col-xs-8 col-sm-8 col-md-8">
            </form>
		  </div>
		 </div>
		</section>
		
<div class="mt-5"></div>		
		
		
		
		
		
		
		
		
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</body>
</html>
