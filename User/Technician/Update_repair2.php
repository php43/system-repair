<?php
 session_start();
 error_reporting(0);
 if($_SESSION['emp_id']=="")
 {
	echo "<script>";
  	echo "alert(\"กรุณาล็อกอิน\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 elseif($_SESSION['status']!="Technician")
 {
	echo "<script>";
  	echo "alert(\"page นี้สำหรับ Technician เท่านั้น!\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 include_once("../../db.inc.php");
 $sql = "SELECT * FROM employee Where emp_id = ' ".$_SESSION['emp_id']." '" ;
 $query = mysqli_query($connect,$sql);
 $result = mysqli_fetch_array($query);


 $id = $_GET['repair_id'];
 $sql2  = "SELECT customer.*,product.*, repair.*\n";
 $sql2 .= "FROM customer,product,repair\n";
 $sql2 .= "where product.product_id = repair.product and customer.cus_id = repair.customer and repair.repair_id = '$id'  ";
 $results = mysqli_query($connect,$sql2) or die (mysqli_error($connect));
 $row  = mysqli_fetch_array($results);


?>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<link rel="stylesheet" href="../../css/styless.css">
		<link href="https://fonts.googleapis.com/css?family=Aclonica" rel="stylesheet">
		<title>IT - Support</title>
	</head>
	<body>
<div class="py-1"></div>
  <div class="mx-auto">
  <form  method ="post" class="mr-auto ml-auto ">
  <div class="form-row">
    <div class="mt-1 form-grop col-xs-12 col-sm-12 col-md-12"></div>
       <div class="mt-3 form-group  col-xs-4 col-sm-4 col-md-4">
          <label>ชื่อ:</label>
          <input type="text" class="form-control" name="name" placeholder="ชื่อ" value="<?php echo $row['fname'];?>">
       </div>

       <div class="mt-3 form-group  col-xs-4 col-sm-4 col-md-4">
          <label>นามสกุล:</label>
          <input type="text" class="form-control" name="lname" placeholder="นามสกุล" value="<?php echo $row['lname'];?>">
       </div>

      <div class="mt-3 form-group  col-xs-4 col-sm-4 col-md-4">
          <label> เบอร์โทรศัพท์:</label>
          <input type="tell" class="form-control" name="tell"  maxlength="10" placeholder="เบอร์โทรศัพท์"value="<?php echo $row['tell'];?>">
      </div>

      <div class="form-group  col-xs-12 col-sm-12 col-md-12">
          <label>ที่อยู่:</label>
          <textarea class="form-control" name="adr" id="exampleFormControlTextarea1" rows="3" placeholder="ทีอยู่"><?php echo $row['address'];?></textarea>
     </div>

         <div class="form-group  col-xs-3 col-sm-3 col-md-3">
              <label>ตำบล:</label>
              <input type="text" class="form-control" name="sub"   placeholder="ชื่อตำบล" value="<?php echo $row['subdistrict'];?>">
         </div>

         <div class="form-group  col-xs-3 col-sm-3 col-md-3">
              <label>อำเภอ:</label>
              <input type="text" class="form-control" name="district"   placeholder="ชื่ออำเภอ" value="<?php echo $row['district'];?>">
         </div>

         <div class="form-group  col-xs-3 col-sm-3 col-md-">
              <label>จังหวัด:</label>
              <input type="text" class="form-control" name="prc"   placeholder="ชื่อจังหวัด"  value="<?php echo $row['province'];?>">
         </div>

         <div class="form-group  col-xs-3 col-sm-3 col-md-3">
              <label>รหัสไปรษณีย์:</label>
              <input type="text" class="form-control" name="_code"  maxlength="5" placeholder="รหัสไปรษณีย์" value="<?php echo $row['zipcode'];?>">

         </div>


         <hr class="mt-2" style="border-style: dotted; background:#9C0E10; ">

         <div class="form-group  col-xs-3 col-sm-3 col-md-3">
              <label>ประเภทสินค้า:</label>
              <input type="text" class="form-control" name="cat"   placeholder="ประเภทสินค้า" value="<?php echo $row['category']?>">
         </div>

         <div class="form-group col-xs-3 col-sm-3 col-md-3">
         	  <label>ยี่ห้อสินค้า:</label>
         	  <input type="text" class="form-control" name="brand" placeholder="ยี่ห้อสินค้า" value="<?php echo $row['brand']?>">
         </div>

         <div class="form-group col-xs-3 col-sm-3 col-md-3">
         	  <label>รุ่น:</label>
         	  <input type="text" class="form-control" name="version" placeholder="ยี่ห้อสินค้า" value="<?php echo $row['version']?>">
         </div>

          <div class="form-group col-xs-3 col-sm-3 col-md-3">
			  <label>จำนวนสินค้า <span style="color: red;">(*เครื่อง) </span> :</label>
         	  <input type="number" class="form-control" name="amount" placeholder="จำนวนสินค้า"  value="<?php echo $row['amount']?>">
         </div>

         <div class="form-group col-xs-4 col-sm-4 col-md-4">
         	  <label>วันที่แจ้งซ่อม :</label>
         	  <input type="date" class="form-control" name="date" placeholder="จำนวนสินค้า" value="<?php echo $row['strdate']?>">
         </div>

         <div class="form-group  col-xs-12 col-sm-12 col-md-12">
              <label>ปัญหา:</label>
              <textarea class="form-control" name="prd" id="exampleFormControlTextarea1" rows="3" placeholder="ทีอยู่"><?php echo $row['problem']?></textarea>
         </div>

         <div class="form-grop col-xs-4 col-sm-4 col-md-4">
            <label for="exampleFormControlSelect1">เจ้าหน้าที่รับผิดชอบในการซ่อม</label>
            <select class="form-control" name="listbox">
            <option value="">- - - รายชื่อเจ้าหน้าที่แจ้งซ่อม - - - </option>
            <?php
              $sql3 = "SELECT*FROM employee where Status = 'Technician'  Order By emp_id  ASC";
              $query3 = mysqli_query($connect,$sql3);
              $m = $_SESSION['emp_id'];

              while($row1 =mysqli_fetch_array($query3)){ ?>
               <option value="<?php $row1['emp_id'];?>" <?php if($m == $row1['emp_id'])echo "selected";?> >
                 <?php echo $row1['emp_id']."- - -". $row1['fname']." ".$row1['lname'];?>
               </option>
             <?php }?>
          </select>

         </div>

         <div class="mt-3 form-group  col-xs-12 col-sm-12 col-md-12">
              <label>วิธีการแก้ปัญหา:</label>
              <textarea class="form-control" name="solution"  rows="3" placeholder="ใส่ข้อความการแก้ปัญหา เช่น เปลี่ยนถ่าน bios"><?php echo $row['solution']?></textarea>
         </div>

         <div class="form-grop col-xs-4 col-sm-4 col-md-4">
            <label for="exampleFormControlSelect1">สถานะ</label>
            <select class="form-control" name="status">
            <option value="">- - -  - - - </option>
               <option value="รอซ่อม"<?php if($row['status']=="รอซ่อม")echo "selected";?>>รอซ่อม</option>
               <option value="กำลังซ่อม"<?php if($row['status']=="กำลังซ่อม")echo "selected";?>>กำลังซ่อม</option>
               <option value="ซ่อมเสร็จแล้ว"<?php if($row['status']=="ซ่อมเสร็จแล้ว")echo "selected";?>>ซ่อมเสร็จแล้ว</option>
          </select>
         </div>

         <div class="form-group col-xs-8 col-sm-8 col-md-8">

         </div>



         <div class="mt-4 form-group col-xs-4 col-sm-4 col-md-4">
             <label>วันที่ปิดงานแจ้งซ่อม <span style="color:red;"><br>(* หมายเหตุถ้าสถานะเท่ากับซ่อมเสร็จแล้ว ให้ใส่วันที่ปิดงาน)</span></label>
             <input type="date" class="form-control" name="enddate">
         </div>



         <div class="form-group col-xs-6 col-sm-6 col-md-6">
         </div>





         <div class="py-3 mr-auto col-xs-8 col-sm-8 col-md-8">
             <div class=" mr-auto">
                 <button type="submit" name="submit" class="btn btn-outline-success">Update</button>
                 <button type="reset" name="reset" class="btn btn-outline-danger">Reset</button>
             </div>
         </div>
  </form>

    <?php
        if(isset($_POST["submit"])){
         #ตาราง repair

           $solution = mysqli_real_escape_string($connect,$_POST['solution']);
           $status  = mysqli_real_escape_string($connect,$_POST['status']);
           $enddate  = mysqli_real_escape_string($connect,$_POST['enddate']);

           $update = "UPDATE repair SET solution ='$solution', status ='$status',enddate ='$enddate' Where  repair_id = '$id'";
           $queryup = mysqli_query($connect,$update)or die(mysqli_error($connect));


           if($queryup){
           echo "<script type='text/javascript'>";
           echo "alert('Update Succesfuly');";
           //echo "window.history.back()";
           echo "window.location = 'Waiting_from.php'; ";
           echo "</script>";
         }else{
           echo "<script type='text/javascript'>";
           echo "alert('Error back to Update again');";
           echo "window.history.back()";
           echo "</script>";
         }
     }

  mysqli_close($connect);
    ?>
</div>
</body>
</html>
