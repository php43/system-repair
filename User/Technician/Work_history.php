<?php
 session_start();
 error_reporting(0);
 if($_SESSION['emp_id']=="")
 {
	echo "<script>";
  	echo "alert(\"กรุณาล็อกอิน\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 elseif($_SESSION['status']!="Technician")
 {
	echo "<script>";
  	echo "alert(\"page นี้สำหรับ Technician เท่านั้น!\");";
  	echo "window.location='../../index.php'";
  	echo "</script>";
 }
 include_once("../../db.inc.php");
 $sql = "SELECT * FROM employee Where emp_id = ' ".$_SESSION['emp_id']." '" ;
 $query = mysqli_query($connect,$sql);
 $result = mysqli_fetch_array($query);

 $emp_id  = $_GET["emp_id"];
 $query2  = mysqli_query($connect,"SELECT * FROM employee where emp_id = '$emp_id' ");
 $result2 = mysqli_fetch_array($query2);
?>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

		<link rel="stylesheet" href="../../css/styless.css">
		<link href="https://fonts.googleapis.com/css?family=Aclonica" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script>
          $(document).ready(function() {
            $('#datatable').DataTable( {
                 "lengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]], //แสดงข้อมูลแถวที่ต้องการ เช่น 5 รายการ 10 25 50 100 และแสดงทั้และแสดงทั้งหมด
                  //ปรับภาษา
                 "oLanguage": {
                 "sLengthMenu": "แสดง _MENU_ เร็คคอร์ด ต่อหน้า",
                 "sZeroRecords": "ไม่มีข้อมูล",
                 "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ เร็คคอร์ด",
                 "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 เร็คคอร์ด",
                 "sInfoFiltered": "(จากเร็คคอร์ดทั้งหมด _MAX_ เร็คคอร์ด)",
                 "sSearch": "ค้นหา :",
                }
                } );
            } );
    </script>

		<title>IT - Support</title>
	</head>
	<body>
		<?php include "header.php";?>

		<section class="container">
		 <div class="row">
 		  <div class="mt-2 col-xs-12 col-sm-12 col-md-12 col-lg-12 border " style="background: #FFFFFF;">
 		   	<div class="py-1"></div>
 		   	<div class="font-weight-bold">
 		      	ประวัติงานที่ได้รับหมอบหมาย</div>
			<hr class="mt-1" style="background: goldenrod;">
      <div class="mt-4"></div>

      <?php
      $empid = $_SESSION['emp_id'];
      $sql2  = "SELECT customer.*,product.*, repair.*\n";
      $sql2 .= "FROM customer,product,repair\n";
      $sql2.= "where product.product_id = repair.product and customer.cus_id = repair.customer \n ";
      $sql2 .= "and repair.technician ='$empid'\n";
      $sql2 .= "order by repair.repair_id DESC ";
      $results = mysqli_query($connect,$sql2) or die (mysqli_error($connect));

       ?>
     <table  border="0" align="center" cellspacing="1" class=" table table-bordered table-hover " id="datatable">
            <!--ส่วนหัว-->
      <thead>
        <tr align="center">
          <th align="center" width="50">วันที่แจ้งซ่อม</th>
          <th align="center" width="100px">รหัสลูกค้า</th>
          <th align="center" width="100px">รหัสสินค้า</th>
          <th align="center" width="200px">รหัสฟอร์มแจ้งซ่อม</th>
          <th align="center" width="10px">View</th>

       </tr>
    </thead>
    <?php while($row = mysqli_fetch_array($results))  {?>
       <tr align="center">
           <td><?php  echo date("d/m/Y",strtotime($row ['strdate']));?></td>
           <td><a href="viewcustomer1.php?cus_id=<?php echo $row['cus_id'];?>" target="_blank"><?php echo $row['cus_id'];?></a></td>
           <td><a href="viewproduct.php?product_id=<?php echo $row['product_id'];?>"  target="_blank"><?php echo $row['product'];?></a></td>
           <td><a href="viewrepair.php?repair_id=<?php echo $row['repair_id'];?>"  target="_blank"><?php echo $row['repair_id'];?></a></td>
           <td>
            <a class="btn-sm btn-outline-info"href="viewProfileCustomer.php?repair_id=<?php echo $row['repair_id'];?>"onclick="return confirm('ต้องการดูข้อมูลประวัติการซ่อม')">View</a>
           </td>
        </tr>
            <?php }?>
          </table>
		  </div>
		 </div>
		</section>

<div class="mt-5"></div>









	</body>
</html>
