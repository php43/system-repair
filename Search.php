<?php
 session_start();
 error_reporting(0);
 if($_SESSION['status']=="Owner")
 {
	 header('Location: User/Owner/Owner_page.php');
	 exit(0);

 }elseif($_SESSION['status']=="Manager")
 {
	 header('Location: User/Manager/Manager_page.php');
	 exit(0);

 }elseif($_SESSION['status']=="Employee")
 {
	 header('Location: User/Employee/Employee_page.php');
	 exit(0);

 }elseif($_SESSION['status']=="Technician")
 {
	 header('Location: User/Technician/Technician_page.php');
	  exit(0);
 }
?>
<!doctype html>
<html lang="th">
<head>

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" href="css/styless.css">
<!-- google font -->
<link href="https://fonts.googleapis.com/css?family=Aclonica" rel="stylesheet">
<!-- script bootstrap-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<title>IT - Support</title>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light " style="background: #FCFCFC;">
 <a class="navbar-brand text-white ml-5" href="index.php">
 <span class="ml-5 " style="color: #B3B0B0;">
 <img src="imgaes/tools.png" height="40">
      IT - Support</span>
 </a>
 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarit2" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
 <span class="navbar-toggler-icon"></span>
 </button>

 <div class="collapse navbar-collapse" id="navbarit2">

  <ul class="navbar-nav ml-auto mr-5">
  	<li class="nav-item active ">
  	 <a class="nav-link active" href="index.php">
		 <span style="color: goldenrod;"> หน้าแรก </span>
		 <span class="sr-only"></span>
	 </a>
  	</li>

  </ul>
 </div>
</nav>



        <!-- ส่วน login -->

	<div class="py-5">
	<div class="content-fluid">
	<div class ="row">
	<div class ="col-md-3"></div>
	<div class ="mx-auto col-md-10">
	<div class="card text-dark p-5 bg-light border">
	<div class="card-body">

	<h3 class="mb-4 text-center">

	 ค้นหาสินค้า</h3>

	<form  method = "post">
		<div class="form-group">
      <div class="input-group mb-3">
      <input type="text" class="form-control"  name="Search" placeholder="หมายเลขสินค้า เช่น 4231  / 2314 " >
      <div class="input-group-append">
      <input type ="submit" name="S" class="btn btn-outline-info" value="ค้นหา" >
      </div>
		</div>
  </div>
  <?php
   #เชื่อมต่อฐานข้อมูล

   include "db.inc.php";
   error_reporting(0);

     if(isset($_POST['S'])){
     $ql = "SELECT * FROM repair Where order_no LIKE '".$_POST['Search']."' ";
     $Query1 = mysqli_query($connect,$ql)or die(mysqli_error($connect));
     $check = mysqli_num_rows($Query1);
  ?>
  <?php if($check ==""){?>  <!-- ตรวจสอบกรณีที่ไม่มีข้อมูลให้แสดงข้อความด้านล่าส-->
    <?php echo "<h1 class='text-center' style='color:red; '>ไม่พบข้อมูลที่ค้นหา</h1>";?>
  <?php }else{  ?>
    <table  class="mt-5 table table-border" border="1" style="font-size:17px;">
      <tr class="text-center">

         <th>รหัสลูกค้า</th>
         <th>รหัสสินค้า</th>
         <th>รหัสเจ้าหน้าที่</th>
         <th>ปัญหา</th>
         <th>สถานะ</th>
         <th>View</th>
      </tr>
      <?php while ($row = mysqli_fetch_array($Query1)) {?>
        <tr class="text-center">
          <td><?php echo $row['customer'];?></td>
          <td><?php echo $row['repair_id'];?></td>
          <td><?php echo $row['employee'];?></td>
          <td><?php echo $row['problem'];?></td>
          <td>
            <?php
               if($row['status']=="รอซ่อม")
               {
                 echo "<div style='background:#ff9900; color: #ffffff;'>รอซ่อม</div>";
               }elseif ($row['status']=="กำลังซ่อม") {
                 echo "<div style='background: #66a3ff; color: #ffffff;'>กำลังซ่อม</div>";
               }else{
                 echo "<div style='background:#1aff1a; color: #ffffff;'>ซ่อมเสร็จแล้ว</div>";
               }
            ?>
         </td>
       <td>
         <a class="btn-sm btn-outline-info"href="viewproduct.php?repair_id=<?php echo $row['repair_id'];?>"onclick="return confirm('ต้องการดูข้อมูลประวัติการซ่อม')" target="_blank">View</a>

       </td>

        </tr>
 </table>
  <?php } } }?>
  <?php mysqli_close($connect);?>
	</form>

	</div><!--end card body-->
	</div><!--end card -->
	</div><!-- end col-md-6-->
	</div><!-- end row-->
	</div><!--end container-->
	</div><!-- end py-5 -->
<!-- จบ login -->

















<div class="py-1"></div>
        <div class=" text-dark bg-light">
          <div class="container">
            <div class="row">

              <div class="col-xs-12 col-sm-12 col-md-12 mt-3 text-center">
                  <p class="text-center">© Copyright 2018 IT - Support</p>
                  <small>ระบบนี้เป็นระบบที่จัดทำขึ้นเพื่อใช้ในในวิชา Advanced web development and design || Advanced database management system and application</small>
                  <small><br>หากมีอะไรผิดพลาดก็ขออภัย ณ ที่นี้ อำเภอ หาดใหญ่ จังหวัด สงขลา 90110</small>
              </div>
            </div>
          </div>
 </div>
</body>
</html>
