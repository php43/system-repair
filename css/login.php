<?php
 session_start();
 error_reporting(0);
 if($_SESSION['status']=="Owner")
 {
	 header('Location: User/Owner/Owner_page.php');
	 exit(0);

 }elseif($_SESSION['status']=="Manager")
 {
	 header('Location: User/Manager/Manager_page.php');
	 exit(0);

 }elseif($_SESSION['status']=="Employee")
 {
	 header('Location: User/Employee/Employee_page.php');
	 exit(0);

 }elseif($_SESSION['status']=="Technician")
 {
	 header('Location: User/Technician/Technician_page.php');
	  exit(0);
 }
?>
<!doctype html>
<html lang="th">
<head>

<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" href="css/styless.css">
<!-- google font -->
<link href="https://fonts.googleapis.com/css?family=Aclonica" rel="stylesheet">
<!-- script bootstrap-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<title>IT - Support</title>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light " style="background: #FCFCFC;">
 <a class="navbar-brand text-white ml-5" href="index.php">
 <span class="ml-5 " style="color: #B3B0B0;">
 <img src="imgaes/tools.png" height="40">
      IT - Support</span>
 </a>
 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarit2" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
 <span class="navbar-toggler-icon"></span>
 </button>

 <div class="collapse navbar-collapse" id="navbarit2">

  <ul class="navbar-nav ml-auto mr-5">
  	<li class="nav-item active ">
  	 <a class="nav-link active" href="index.php">
		 <span style="color: goldenrod;"> หน้าแรก </span>
		 <span class="sr-only"></span>
	 </a>
  	</li>

  </ul>
 </div>
</nav>



        <!-- ส่วน login -->

	<div class="py-5">
	<div class="container">
	<div class ="row">
	<div class ="col-md-3"></div>
	<div class ="col-md-6">
	<div class="card text-dark p-5 bg-light border">
	<div class="card-body">

	<h3 class="mb-4 text-center">
	<img src="imgaes/tools.png" width="50" height="40" class="d-inline-block align-top" alt="">
	 IT - Support</h3>

	<form action="checklogin.php" method = "post">
		<div class="form-group">
			<label>Username</label>
			<input type="text" name = "_userd"  class="form-control" placeholder="Enter Username" required>
		</div>

		<div class="form-group">
			<label class="">Password</label>
			<input type="password"  name="_pass"  class="form-control" placeholder="Password" required>
		</div>

		<div class="mt-1">
		</div>
			<button type="submit" name ="submit"  class="mt-2 btn btn-danger" >login</button>
	</form>

	</div><!--end card body-->
	</div><!--end card -->
	</div><!-- end col-md-6-->
	</div><!-- end row-->
	</div><!--end container-->
	</div><!-- end py-5 -->
<!-- จบ login -->

















<div class="py-1"></div>
        <div class=" text-dark bg-light">
          <div class="container">
            <div class="row">

              <div class="col-xs-12 col-sm-12 col-md-12 mt-3 text-center">
                  <p class="text-center">© Copyright 2018 IT - Support</p>
                  <small>ระบบนี้เป็นระบบที่จัดทำขึ้นเพื่อใช้ในในวิชา Advanced web development and design || Advanced database management system and application</small>
                  <small><br>หากมีอะไรผิดพลาดก็ขออภัย ณ ที่นี้ อำเภอ หาดใหญ่ จังหวัด สงขลา 90110</small>
              </div>
            </div>
          </div>
 </div>
</body>
</html>
