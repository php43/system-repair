<?php
 session_start();
 error_reporting(0);
 if($_SESSION['status']=="Owner")
 {
	 header('Location: User/Owner/Owner_page.php');
	 exit(0);

 }elseif($_SESSION['status']=="Manager")
 {
	 header('Location: User/Manager/Manager_page.php');
	 exit(0);

 }elseif($_SESSION['status']=="Employee")
 {
	 header('Location: User/Employee/Employee_page.php');
	 exit(0);

 }elseif($_SESSION['status']=="Technician")
 {
	 header('Location: User/Technician/Technician_page.php');
	  exit(0);
 }
?>
<!doctype html>
<html lang="th">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<link rel="stylesheet" href="css/styless.css">
<!-- google font -->
<link href="https://fonts.googleapis.com/css?family=Aclonica" rel="stylesheet">
<!-- script bootstrap-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<title>IT - Support</title>
</head>

<body>
<?php include "header.php";?>


<div class="container">
 <div class="row">

 	<div class="row py-5">
        <div class="col-md-5 order-5 order-md-5">
          <img class="img-thumbnail d-block my-3 mx-auto animate-in-left" src="imgaes/tot/1.PNG" >
         </div>

        <div class="col-md-7 align-self-center order-1 order-md-7 my-7 text-md-left text-center">
          <h2>1.เมือได้แบบฟอร์มที่ทำรายการมา</h2>
          <p class="my-4 text-muted"></p>

        </div>
      </div>

      <div class="row pt-5">
        <div class="align-self-center col-lg-5 text-md-left text-center">
          <h2>2.ให้สังเกตุตรงรหัสสินค้า</h2>
          <p class="my-4 text-muted"></p>
        </div>
        <div class="align-self-center mt-7 col-lg-7">
          <img class="img-thumbnail d-block animate-in-right" src="imgaes/tot/2.png"> </div>
      </div>

      <div class="row py-5">
            <div class="col-md-5 order-5 order-md-5">
              <img class="img-thumbnail d-block my-3 mx-auto animate-in-left" src="imgaes/tot/3.PNG" >
             </div>

            <div class="col-md-7 align-self-center order-1 order-md-7 my-7 text-md-left text-center">
              <h2>3.ให้ไปที่ค้นหา</h2>
              <p class="my-4 text-muted"></p>

            </div>
          </div>

 </div>
</div>

<!-- footer -->
<div class="py-5"></div>
     <div class=" text-dark bg-light">
     <div class="container">
    <div class="row">
   <div class="col-xs-12 col-sm-12 col-md-12 mt-3 text-center">
    <p class="text-center">© Copyright 2018 IT - Support</p>
 <small>ระบบนี้เป็นระบบที่จัดทำขึ้นเพื่อใช้ในในวิชา Advanced web development and design || Advanced database management system and application</small>
 <small><br>หากมีอะไรผิดพลาดก็ขออภัย ณ ที่นี้ อำเภอ หาดใหญ่ จังหวัด สงขลา 90110</small>
   </div>
  </div>
  </div>
</div>
</body>
</html>
