-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2018 at 04:01 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `repair`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `cus_id` int(4) UNSIGNED ZEROFILL NOT NULL,
  `fname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tell` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `subdistrict` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `district` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `province` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(5) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`cus_id`, `fname`, `lname`, `tell`, `address`, `subdistrict`, `district`, `province`, `zipcode`) VALUES
(0001, 'ณัฐวุฒิ', 'ขุนตำลิ', '0918462315', '1563 ชอย 49', 'หาดใหญ่', 'หาดใหญ่', 'สงขลา', '90110');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `emp_id` int(4) UNSIGNED ZEROFILL NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sex` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `tell` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `subdistrict` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `district` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `province` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Owner','Manager','Employee','Technician') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Employee',
  `mng_id` int(4) UNSIGNED ZEROFILL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`emp_id`, `username`, `password`, `fname`, `lname`, `sex`, `tell`, `email`, `address`, `subdistrict`, `district`, `province`, `zipcode`, `status`, `mng_id`) VALUES
(0001, 'admin', 'admin', 'admin', 'IT-SUPPORT', 'M', '0918462315', 'NATTAWUT@GMAIL.COM', '  -', '-', '-', '-', '-', 'Owner', NULL),
(0002, '6010210452', '1234', 'ณัฐวุฒิ', 'ขุนตำลิ', 'M', '0918462315', '6010210452@psu.ac.th', '   1563', 'หาดใหญ่', 'หาดใหญ่', 'สงขลา', '90110', 'Employee', NULL),
(0003, '6010210420', '1234', 'ณัฐพล', 'ขุนตำลิ', 'M', '0918462312', '6010210420@psu.ac.th', '    -\\\\\\\\\\\\\\\\r\\\\\\\\\\\\\\\\n-', 'หาดใหญ่', 'หาดใหญ่', 'สงขลา', '90110', 'Technician', NULL),
(0004, 'A', 'A', 'A', 'A', 'M', '1', 'A', '2', '1', '1', '1', '1', 'Employee', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(4) UNSIGNED ZEROFILL NOT NULL,
  `category` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `amount` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `category`, `brand`, `version`, `amount`) VALUES
(0001, 'คอมพิวเตอร์', 'ASUS', 'X455LD', 1),
(0002, 'Software', 'asus', 'x455ld', 1),
(0003, 'คอมพิวเตอร์PC', 'ASUS', 'V6XLLD', 1);

-- --------------------------------------------------------

--
-- Table structure for table `repair`
--

CREATE TABLE `repair` (
  `repair_id` int(4) UNSIGNED ZEROFILL NOT NULL,
  `order_no` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `customer` int(4) UNSIGNED ZEROFILL NOT NULL,
  `product` int(4) UNSIGNED ZEROFILL NOT NULL,
  `employee` int(4) UNSIGNED ZEROFILL NOT NULL,
  `technician` int(4) UNSIGNED ZEROFILL DEFAULT '0000',
  `problem` text COLLATE utf8_unicode_ci NOT NULL,
  `solution` text COLLATE utf8_unicode_ci,
  `status` enum('รอซ่อม','กำลังซ่อม','ซ่อมเสร็จแล้ว','') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'รอซ่อม',
  `strdate` date NOT NULL,
  `enddate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `repair`
--

INSERT INTO `repair` (`repair_id`, `order_no`, `customer`, `product`, `employee`, `technician`, `problem`, `solution`, `status`, `strdate`, `enddate`) VALUES
(0001, '2907', 0001, 0001, 0004, 0002, '-เพิ่ม RAM 32 GB\r\n-เปลี่ยน CPU เป็น i7', NULL, 'รอซ่อม', '2018-04-15', NULL),
(0002, '9704', 0001, 0002, 0002, 0004, 'ต้องการโปรแกรมเขียนแบบเว็บไชต์', NULL, 'รอซ่อม', '2018-04-15', NULL),
(0003, '7350', 0001, 0003, 0002, 0004, 'เปิดไม่ติดมีเสียงร้องดังตี๊ต', NULL, 'รอซ่อม', '2018-04-14', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`cus_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `repair`
--
ALTER TABLE `repair`
  ADD PRIMARY KEY (`repair_id`),
  ADD UNIQUE KEY `product` (`product`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `cus_id` int(4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `emp_id` int(4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `repair`
--
ALTER TABLE `repair`
  MODIFY `repair_id` int(4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
